/*
    This sketch sends data via HTTP GET requests to data.sparkfun.com service.
    You need to get streamId and privateKey at data.sparkfun.com and paste them
    below. Or just customize this script to talk to other HTTP servers.
*/

#include <ESP8266WiFi.h>

#define engine_1_1 5
#define engine_1_2 4
#define engine_2_1 0
#define engine_2_2 2

void moveUp(){
  digitalWrite(engine_1_1, HIGH);
  digitalWrite(engine_1_2, LOW);
}

void moveDown(){
  digitalWrite(engine_1_1, LOW);
  digitalWrite(engine_1_2, HIGH);
}

void stopTurning(){
  digitalWrite(engine_2_1, LOW);
  digitalWrite(engine_2_2, LOW);
}

void turnLeft(){
  digitalWrite(engine_2_1, HIGH);
  digitalWrite(engine_2_2, LOW);
}

void turnRight(){
  digitalWrite(engine_2_1, LOW);
  digitalWrite(engine_2_2, HIGH);
}

void stopHere(){
  digitalWrite(engine_1_1, LOW);
  digitalWrite(engine_1_2, LOW);
}

const char* ssid     = "wifi name";
const char* password = "password";
const char* host = "192.168.100.101";
WiFiClient client;
void setup() {
  pinMode(engine_1_1, OUTPUT);
  pinMode(engine_1_2, OUTPUT);
  pinMode(engine_2_1, OUTPUT);
  pinMode(engine_2_2, OUTPUT);
  digitalWrite(engine_1_1, LOW);
  digitalWrite(engine_1_2, LOW);
  digitalWrite(engine_2_1, LOW);
  digitalWrite(engine_2_2, LOW);
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections

  const int httpPort = 3000;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // This will send the request to the server
  client.print("car");
}

//int value = 0;

void loop() {
  //delay(50);
 // ++value;

  unsigned long timeout = millis();
  if (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  if (client.available()>0) {
    String line = client.readStringUntil('\n');
   // Serial.println(client.available());
    Serial.println(line);
    int command = line.toInt();
    switch(command){
      case 5:
        stopTurning();
        stopHere();
        break;
      case 8:
        stopTurning();
        moveUp();
        break;
      case 2:
        stopTurning();
        moveDown();
        break;
      case 6:
        stopHere();
        turnRight();
        break;
      case 4:
        stopHere();
        turnLeft();
        break;
      case 9:
        turnRight();
        moveUp();
        break;
      case 7:
        turnLeft();
        moveUp();
        break;
      case 1:
        turnLeft();
        moveDown();
        break;
      case 3:
        turnRight();
        moveDown();
        break;
      default:
        break;      
    }
  }

}
