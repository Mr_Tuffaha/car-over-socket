<?php

/**
* code originaly copied from : http://www.binarytides.com/php-socket-programming-tutorial/
* edited by: Omar Tuffaha
* @Version: 2.3v
*/
error_reporting(~E_NOTICE);

//setting time limit to 0 to keep server alive
set_time_limit(5*60*1000);

//initialising address and port and max clients
//$address = "127.0.0.1";
$address = "0.0.0.0";
$port = 3000;
$max_clients = 2;

function initSocket(){
    //create a socket and check for errors
    if (!($socket = socket_create(AF_INET, SOCK_STREAM, 0))) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);
        die("Couldn't create socket: [$errorcode] $errormsg \n");
    }else{
        //print that the socket has been created
        echo "Socket created \n";
        return $socket;
    }

}

function bindSocketToPort($socket,$address,$port){
    if (!socket_bind($socket, $address, $port)) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);
        die("Could not bind socket : [$errorcode] $errormsg \n");

        return false;
    }else{
        //print the socket has been binded
        echo "Socket bind OK to port $port \n";
        return true;
    }
}

function setListenLimit($socket,$max_clients){
    //sets how many clients can be recieved from (put in que) and check for errors
    if (!socket_listen($socket, $max_clients)) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);
        die("Could not listen on socket : [$errorcode] $errormsg \n");
    }//end of if()

    echo "Socket listen OK \n";

}

function addClientsSockets($client_socks,$max_clients,$socket){
    //prepare array of readable client sockets
    $read = array();

    //first socket is the master socket
    $read[0] = $socket;

    //now add the existing client sockets
    for ($i = 0; $i < $max_clients; $i++) {
        if ($client_socks[$i]['socket'] != null) {
            $read[$i + 1] = $client_socks[$i]['socket'];
        }//end of if
    }//end of for loop
    return $read;
}

$socket = initSocket();
while(!bindSocketToPort($socket,$address,$port));
setListenLimit($socket,$max_clients);

echo "Waiting for incoming connections... \n";

//array of client sockets
$client_socks = array();
//start loop to listen for incoming connections and process existing connections
while (true) {
    $messageData = array();
    $read = addClientsSockets($client_socks,$max_clients,$socket);
    //now call select - blocking call
    if (socket_select($read, $write, $except, null) === false) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);

        die("Could not listen on socket : [$errorcode] $errormsg \n");
    }//end of if()

    //if read contains the master socket, then a new connection has come in
    if (in_array($socket, $read)) {
        for ($i = 0; $i < $max_clients; $i++) {
            //if the $client_socks not set then set the new one to it
            if ($client_socks[$i]['socket'] == null) {
                $client_socks[$i]['socket'] = socket_accept($socket);

                /**
                *    here put what will the socket do after adding the new connection
                */
                //socket_write($client_socks[$i]['socket'], "name\n");
                //print the client info
                if (socket_getpeername($client_socks[$i]['socket'], $caddress, $cport)) {
                    echo "Client $caddress : $cport is now connected to us. \n";
                    $client_socks[$i]['ip']=$caddress;
                }//end of if()
                break;
            }//end of if()
        }//end of for loop
    }//end of if()

    //check each client if they send any data
    for ($i = 0; $i < $max_clients; $i++) {
        //if the $client_socks is set meaning it exists read its input
        if (in_array($client_socks[$i]['socket'], $read)) {
            $input = socket_read($client_socks[$i]['socket'], 1024);

            //if the $client_socks has no name assign the input to the name
            if (!isset($client_socks[$i]['name'])) {
                $client_socks[$i]['name'] = rtrim($input);
                //socket_write($client_socks[$i]['socket'], "thanks " . $client_socks[$i]['name'] . "\n");
                break;
            }//end of if

            //if no input meaning client is lost
            if ($input == null) {
                //zero length string meaning disconnected, remove and close the socket
		echo "Disconnected: ".$client_socks[$i]['ip'];
                socket_close($client_socks[$i]['socket']);
                unset($client_socks[$i]);
                $client_socks = array_values($client_socks);
                break;
            }//end of if


            $input = rtrim($input);
            //  $n = $input;
            echo "recieved \"$input\" from user " . $client_socks[$i]['name'] . "\n";
            //if the input is 'q' then close connection with this client
            if ($input == "q") {
                //socket_write($client_socks[$i]['socket'], "bye...\n");
                socket_close($client_socks[$i]['socket']);
                echo "user: '" . $client_socks[$i]['name'] . "' has disconnected\n";
                unset($client_socks[$i]);
                //refix the $client_socks array
                $client_socks = array_values($client_socks);
                break;
            }//end of if


            //if the input is 'close' then close connection with all clients
            //and turn off the server
            if ($input == "close") {
                foreach ($client_socks as $client) {
                    //socket_write($client['socket'], "bye...\n");
                    socket_close($client['socket']);
                    echo "user: '" . $client['name'] . "' has disconnected\n";
                }//end of foreach
                unset($client_socks);
                socket_close($socket);
                break 2;
            }//end of if


            // $output = "OK ... $input";

            // echo "Sending output to client \"$input\" \n";
            //send response to client
            // socket_write($client_socks[$i]['socket'], $output);
            array_push($messageData, array('a',$client_socks[$i]['name'],$input));

        }//end of if
    }//end of for loop
    sendData($messageData,$client_socks);
    // sleep(2);
}//end of while(true)

function sendData($data,$client_socks){
    $row = $data[0];
    while(array_shift($data)){
        echo "User {$row[1]}: {$row[2]} -- to all\n";
        foreach ($client_socks as $client) {
            if($client['name']=="car")
            socket_write($client['socket'],$row[2][0]."\n");
        }
        $row = $data[0];
    }
}

//handshake new client. builds up the handshake response
function perform_handshaking($receved_header, $client_conn, $host, $port) {
    $headers = array();
    $lines = preg_split("/\r\n/", $receved_header);
    foreach ($lines as $line) {
        $line = chop($line);
        if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
            $headers[$matches[1]] = $matches[2];
        }
    }

    $secKey = $headers['Sec-WebSocket-Key'];
    $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
    //hand shaking header
    $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
    "Upgrade: websocket\r\n" .
    "Connection: Upgrade\r\n" .
    "WebSocket-Origin: $host\r\n" .
    "WebSocket-Location: ws://$host:$port/server.php\r\n" .
    "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
    socket_write($client_conn, $upgrade, strlen($upgrade));
}





class socket{
    private $address = "127.0.0.1";
    private $port = 5000;
    private $max_clients = 10;
    private $socket = null;
}

?>
