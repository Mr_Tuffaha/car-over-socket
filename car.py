#!/usr/bin/python3.6
import os
import tkinter
import socket

socketControl = socket.socket()         # Create a socket object
host = "127.0.0.1" # Get local machine name
port = 3000                # Reserve a port for your service.
socketControl.connect((host, port))
socketControl.send(str('control').encode("utf-8"))



os.system('xset r off')
color="#FFF"
refreshRate=100
window = tkinter.Tk()
upPressed=0
downPressed=0
rightPressed=0
leftPressed=0

def sendCommand():
    keysCount=upPressed+downPressed+rightPressed+leftPressed
    command=""
    if(keysCount==0):
        command="5"
    if (keysCount == 1):
        if(upPressed):
            command="8"
        elif(downPressed):
            command="2"
        elif(rightPressed):
            command="6"
        elif(leftPressed):
            command="4"
    elif(keysCount==2):
        if (upPressed and rightPressed):
            command = "9"
        elif (upPressed and leftPressed):
            command = "7"
        elif (downPressed and rightPressed):
            command = "3"
        elif (downPressed and leftPressed):
            command = "1"
    print(str(command).encode("utf-8"))
    socketControl.send(str(command).encode("utf-8"))

def keyup(e):
    change=0
    global upPressed
    global downPressed
    global rightPressed
    global leftPressed
    if(e.keycode==111 and upPressed):
        upPressed = 0
        up.config(bg="#FFF")
        change = 1
    if(e.keycode==116 and downPressed):
        downPressed = 0
        down.config(bg="#FFF")
        change = 1
    if(e.keycode==114 and rightPressed):
        rightPressed=0
        right.config(bg="#FFF")
        change = 1
    if(e.keycode==113 and leftPressed):
        leftPressed=0
        left.config(bg="#FFF")
        change = 1
    if( change ):
        sendCommand()

def keydown(e):
    change=0
    global upPressed
    global downPressed
    global rightPressed
    global leftPressed
    if (e.keycode == 111 and downPressed==0):
        upPressed=1
        up.config(bg="#0F0")
        change=1
    if (e.keycode == 116 and upPressed==0):
        downPressed=1
        down.config(bg="#0F0")
        change=1
    if (e.keycode == 114 and leftPressed==0):
        rightPressed=1
        right.config(bg="#0F0")
        change=1
    if (e.keycode == 113 and rightPressed==0):
        leftPressed=1
        left.config(bg="#0F0")
        change=1
    if(change):
        sendCommand()


def refresh_loop():
    window.after(refreshRate, refresh_loop)  # <== re-schedule add_a
    # window.destroy()
    window.update()


up = tkinter.Label(window, text="up", bg=color, font=("Arial Bold", 50))
up.grid(column=1, row=0, padx=5, pady=5)
down = tkinter.Label(window, text="down", bg=color, font=("Arial Bold", 50))
down.grid(column=1, row=1, padx=5, pady=5)
right = tkinter.Label(window, text="right", bg=color, font=("Arial Bold", 50))
right.grid(column=2, row=1, padx=5, pady=5)
left = tkinter.Label(window, text="left", bg=color, font=("Arial Bold", 50))
left.grid(column=0, row=1, padx=5, pady=5)

# Code to add widgets will go here...
window.bind("<KeyPress>", keydown)
window.bind("<KeyRelease>", keyup)

window.after(refreshRate, refresh_loop)
window.mainloop()

socketControl.close
os.system('xset r on')
